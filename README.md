# PH Covid 19 Backend Python

This is the Backend Python setup for the project.

# Installation

We should use an environment of Python 3

```bash
virtualenv env -p python3 # as long as python3 is python 3
source env/bin/activate
pip install -r requirements.txt
```

# Usage

Put your usage here or how to run this system

# Documentation

Please visit the repo page or if you want to build your own copy here, you have to install Hugo globally

```bash
brew install hugo
```

Then run the following commands:

```bash
npm run serve-docs
```

If you want to just build the docs,

```bash
npm run build-docs
```
