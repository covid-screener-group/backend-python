---
title: "Installation"
date: 2020-03-03
draft: false
weight: 10
---

You should have the following:
- Node version 10 or 12

To install run,
```bash
npm install
```

This is for commitlint to work.

We should use an environment of Python 3

```bash
virtualenv env -p python3 # as long as python3 is python 3
source env/bin/activate
pip install -r requirements.txt
```
